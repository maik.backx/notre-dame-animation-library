var notreDame = (function() {
  my = {}; //In de main.js wordt deze naam opgeroepen om de functie te activeren.

  // *** Private properties *** //
  var procent       = 0; //Bouwproces van de Notre Dame Var onderdeel //Onderdeel van de Notre Dame

  // *** Public properties *** //
  my.textline= "";

  // *** Public functions *** //
  my.sceneDialog = function(procent) {
    switch (procent) {
      case 10:
        textline = "The cathedral's construction was begun in 1160 under Bishop Maurice de Sully";    //Als de variabele procent op waarde 10 staat laat deze regel text zien.
        up_animation();
        down_animation();
        setTimeout(left_to_right_apostelen_animation, 2500);
        setTimeout(left_to_right_gate_animation, 4300);
        break;
      case 20:
        textline = "The cathedral is consecrated to the Virgin Mary and considered to be one of the finest examples of French Gothic architecture. ";
        up_animation();
        break;
      case 30:
        textline = "Popular interest in the cathedral blossomed soon after the publication, in 1831, of Victor Hugo's novel Notre-Dame de Paris (better known in English as The Hunchback of Notre-Dame).";
        up_animation();
        up_down_animation();
        setTimeout(rotate_right_animation, 1000);
        setTimeout(rotate_left_animation, 2000);
        setTimeout(rotate_right_animation, 3000);
        setTimeout(rotate_left_animation, 4000);
        setTimeout(rotate_right_animation, 5000);
        setTimeout(rotate_left_animation, 6000);
        setTimeout(rotate_right_animation, 7000);
        setTimeout(rotate_left_animation, 8000);
        break;
      case 40:
        textline = "In 1805, Notre-Dame was given the honorary status of a minor basilica.";
        up_animation();
        break;
      case 50:
        textline = "This led to a major restoration project between 1844 and 1864, supervised by Eugène Viollet-le-Duc.";
        up_animation();
        break;
      case 60:
        textline = "The liberation of Paris was celebrated within Notre-Dame in 1944 with the singing of the Magnificat.";
        up_animation();
        right_to_left_animation();
        break;
      case 70:
        textline = "The cathedral is one of the most widely recognized symbols of the city of Paris and the French nation.";
        up_animation();
        break;
      case 80:
        textline = "Beginning in 1963, the cathedral's façade was cleaned of centuries of soot and grime.";
        up_animation();
        break;
      case 90:
        textline = "On 15th of April 2019 the Notre-Dame was burning for around 15 hours, the cathedral sustained serious damage.";
        up_animation();
        up_fire_animation();
        break;
      case 100:
        textline = "But after many years we know one thing for sure. The Notre Dame, the soul of France, will never die.";
        up_animation();
        down_fire_animation();
        opacity_animation();
        break;
      default:
    }
    document.getElementById("textOutput").innerHTML = textline;
  }

  // *** Private functions *** //
  function up_animation() {
    var positionBigHall = $("#NotreDameBigHall").css('bottom', '+=25');  //Jquery ondersteunenend regel code
    var positionSideTower = $("#NotreDameSideTower").css('bottom', '+=24.7'); //De zijtoren van de Notre Dame gaat 24.7 pixels omhoog vanaf de bottom.
    var positionCenterTower = $("#NotreDameCenterTower").css('bottom', '+=42.5');
    var positionTowers = $("#NotreDameTowers").css('bottom', '+=45');
  }

  function up_fire_animation() {
    var flames = $("#flames").animate({bottom: "+=320px"}, 2000);
  }

  function down_animation() {
    var gate_part1 = $("#apostelen-gate-part1").animate({top: "+=574px"}, 2000);
    var gate_part2 = $("#apostelen-gate-part2").animate({top: "+=574px"}, 2000);
  }

  function down_fire_animation() {
    var flames = $("#flames").animate({bottom: "-=120px"}, 2000);
  }

  function left_to_right_apostelen_animation() {
    var apostelen = $("#apostelen").animate({left: "+=1260px"}, 2500);
  }

  function left_to_right_gate_animation() {
    var gate = $(".gate").animate({left: "+=150px"}, 2000);
  }

  function right_to_left_animation() {
    var planes = $(".planes").animate({right: "+=1500px"}, 5000);
  }

  function up_down_animation() {
    var quasimodoAPPEAR = $("#Quasimodo").animate({top: "+=380px"}, 2500);
    var bellsAPPEAR = $(".bell").animate({top: "+=160px"}, 2500);
    for (var i = 0; i < 1; i++) {
      var quasimodoUP = $("#Quasimodo").animate({top: "-=50px"}, 2500);
      var quasimodoDOWN = $("#Quasimodo").animate({top: "+=50px"}, 2500);
      var bells = $(".bell").animate({top: "-=0px"}, 2500);
    }
    var quasimodoDISAPPEAR = $("#Quasimodo").animate({top: "-=380px"}, 2500);
    var bellsDISAPPEAR = $(".bell").animate({top: "-=160px"}, 2500);

  }

  function rotate_left_animation() {
    $("#BelLinks").css({'transform' : 'rotate('+ 20 +'deg)'});
    $("#BelRechts").css({'transform' : 'rotate('+ 20 +'deg)'});
  }

  function rotate_right_animation() {
    $("#BelLinks").css({'transform' : 'rotate('+ -20 +'deg)'});
    $("#BelRechts").css({'transform' : 'rotate('+ -20 +'deg)'});
  }

  function opacity_animation() {
    var french_flag = $("#french_flag").animate({opacity: "1"}, 2000);  //Jquery ondersteunenend regel code
  }
}());
