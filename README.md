Notre Dame Animation Library

De Notre Dame Animation Library is een library waarbij de gebruiker zelf een animatie verhaaltje kan maken zonder veel te hoeven coderen.

Installatie
1. Download de externe library jquery.js
2. Download het project van GitLab.
3. Voeg de animatie_library aan je project toe.
4. Voeg aan jouw main.js de code van de main.js van het gedownloade project.

De SWITCH en verhalenverteller
- In de library is de switch de evenementen-organisator. De evenementen worden geactiveerd door deze switch. Ook wordt het verhaal verteld wat je wilt vertellen via de switch verteld.
- Vertel per regel je verhaal via de textline.

Animaties
- De Animatie wordt aangestuurd doormiddel van id's of classes die in jouw html-pagina staan.
- Als je het plaatje 600pixels wilt verplaatsen naar rechts vertel je in de left_to_right_animation "left: "+=600px"".
- Als je de snelheid van de animatie wilt aanpassen pas je de tijd aan van bijvoorbeeld "2000" naar "2500" miliseconden.

Have Fun